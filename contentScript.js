(function () {
    'use strict';

    var specificWords = {
        'quiconque': 'celui qui',
        'celleux': 'ceux',
        'elleux' : 'ceux',
        'iels': 'ils',
        'iel': 'il',
        'ael': 'il',
        'lel': 'il',
        'ul': 'il',
        'ol': 'il',
        'ce·tte': 'ce',
        'celui·elle': 'celui',
        'il·elle': 'il',
        'lui·elle': 'lui',
        'la·le': 'le',
        'un·e': 'un',
        'mon·ma': 'mon',
        'mien·ne': 'mien',
        'tout·e·s': 'tous',
        'chacun·e': 'chacun',
        'individu·e': 'individu',
        'auteur·e·s': 'auteurs',
        'auteur·e': 'auteur',
        'tout·e': 'tout',
        'ouvert·e·s': 'ouverts',
        'inclusif': 'inclusif',
        'équitable': 'équitable',
        'respectueux': 'respectueux',
        'collaborateur·trice': 'collaborateur',
        'lecteur·rice·s': 'lecteurs'
    };

    var texts = document.evaluate('//body//text()[ normalize-space(.) != "" ]', document, null, 6, null);
    var text, i;
    for (i = 0; text = texts.snapshotItem(i); i += 1) {
        var data = text.data;

        for (const [inclusive, standard] of Object.entries(specificWords)) {
            data = data.replace(new RegExp("\\b" + inclusive + "\\b", "gi"), standard);
        }

        text.data = data;
    }
})();
